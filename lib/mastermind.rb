class Code
  attr_reader :pegs

  PEGS = { 0 => 'r', 1 => 'g', 2 => 'b',
           3 => 'y', 4 => 'o', 5 => 'p' }.freeze

  def initialize(pegs_array)
    @pegs = pegs_array
  end

  def self.parse(input)
    input.downcase!
    input.each_char { |ch| raise 'invalid input' unless PEGS.value?(ch) }
    raise 'invalid input' unless input.length == 4
    Code.new(input.split(''))
  end

  def self.random
    rand_code = []
    4.times { rand_code << PEGS[rand(0..5)] }
    Code.new(rand_code)
  end

  def exact_matches(other_code)
    matches = 0
    @pegs.each_with_index { |peg, i| matches += 1 if other_code.pegs[i] == peg }
    matches
  end

  def near_matches(other_code)
    matches = 0
    PEGS.each_value do |color|
      matches += [@pegs.count(color), other_code.pegs.count(color)].min
    end
    matches - exact_matches(other_code)
  end

  def ==(other)
    exact_matches(other) == 4 if other.class == Code
  end

  def [](peg_values)
    @pegs = peg_values
  end

  def to_s
    @pegs.join('')
  end
end

class Game
  attr_reader :secret_code, :guess_count

  def initialize(code = Code.random)
    @secret_code = code
    @guess_count = 0
  end

  def display_matches(guess)
    puts "#{@secret_code.exact_matches(guess)} exact matches"
    puts "#{@secret_code.near_matches(guess)} near matches"
  end

  def get_guess
    puts 'Enter a guess (peg colors are: r, g, b, y, o, p)'
    Code.parse(gets.chomp)
  end

  def winner?(guess)
    guess == @secret_code
  end

  def play
    puts '------------------MASTERMIND---------------------'
    puts 'You have 10 tries to guess the secret code.'
    puts 'The code is made of 4 pegs of the following colors chosen at random:'
    puts 'Red, Orange, Yellow, Green, Blue, and Purple'
    puts 'Guess using the first letter of the color ex: rrrr or ygob'
    guess = ''
    until @guess_count > 9 || winner?(guess)
      guess = get_guess
      @guess_count += 1
      display_matches(guess)
    end
    if winner?(guess)
      puts "Congratulations! You guessed the code in #{@guess_count} tries"
    else
      puts "Your 10 tries are up. The code was #{@secret_code}."
    end
  end
end

if $PROGRAM_NAME == __FILE__
  game = Game.new
  game.play
end
